.PHONY: all console clean new

###
### Common definitions
###

# Project
PROJECT_NAME = RTS-Lab
PROJECT_DIR  = .

# Paths
CONSOLE   = $(PROJECT_DIR)/console
TOOLCHAIN =
#^ if ARM GNU Toolchain is not on PATH, specify its full path including trailing
#^ slash, e.g. $(HOME)/Downloads/arm-gnu-toolchain/bin/

# Directories
BUILD_DIR = $(PROJECT_DIR)/build
OBJ_DIR   = $(BUILD_DIR)/obj
SRC_DIRS  = $(addprefix $(PROJECT_DIR)/,\
            driver/src src lib/src lib/tinytimber/src lib/md407/src)
INC_DIRS  = $(SRC_DIRS:src=inc) $(PROJECT_DIR)/device/inc
OBJ_DIRS  = $(sort $(dir $(OBJS)))

# Source files are automatically detected in directories defined by SRC_DIRS
SRCS = $(foreach d, $(SRC_DIRS), $(wildcard $(d)/*.c) $(wildcard $(d)/*.s))
OBJS = $(SRCS:%=$(OBJ_DIR)/%.o)
DEPS = $(OBJS:.o=.d)

# Target output
TARGET_ELF = $(BUILD_DIR)/$(PROJECT_NAME).elf
TARGET_S19 = $(BUILD_DIR)/$(PROJECT_NAME).s19

# Target RAM
LINKER_SCRIPT = $(PROJECT_DIR)/md407-ram.x

# Compiler
CC   = $(TOOLCHAIN)arm-none-eabi-gcc
POST = $(TOOLCHAIN)arm-none-eabi-objcopy

# Flags
CFLAGS += -O0 \
          -MMD \
          -Wall \
          -mthumb \
          -mcpu=cortex-m4 \
          -mfloat-abi=hard \
          -mfpu=fpv4-sp-d16 \
          -D STM32F40_41xxx \
          $(addprefix -I, $(INC_DIRS))

LDFLAGS += -specs=nano.specs \
           -nostartfiles \
           -mthumb \
           -mfloat-abi=hard \
           -mfpu=fpv4-sp-d16 \
           -mcpu=cortex-m4 \
           -T $(LINKER_SCRIPT) \
           -lm

POSTFLAGS += -S -O srec

###
### Main targets
###

all: $(TARGET_S19)

console: all
	$(CONSOLE) $(TARGET_S19)

clean:
	$(RM) -r $(BUILD_DIR)

new: clean all

###
### Intermediate targets
###

$(TARGET_S19): $(TARGET_ELF)
	$(POST) $(POSTFLAGS) $< $@

$(TARGET_ELF): $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $^

%/:
	mkdir -p $@

.SECONDEXPANSION:
$(OBJ_DIR)/%.o: % | $$(@D)/
	$(CC) $(CFLAGS) -c $< -o $@

-include $(DEPS)
