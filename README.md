# rts_lab_utils
Development environment utilities for the laboratory assignment of Real-time
Systems (LET627, EDA223, DIT162) at Chalmers University of Technology.

The utilities facilitate compiling the project and communicating with the MD407.

1) Install dependencies under [Prerequisites](#prerequisites)
2) Follow instructions under [Usage](#usage)

## Prerequisites
Basic knowledge of how to use a terminal is needed. If you are entirely new to
it, use your favourite search engine to find a tutorial.

### Linux
The dependencies -- Arm GNU Toolchain and GNU Make -- are readily available on
most Linux distributions, though the package names may differ. Below is the
installation command on Debian and its derivatives.

```bash
sudo apt install gcc-arm-none-eabi make
```

### MacOS
Install [Homebrew](https://brew.sh), and run the installation command below.

```bash
brew install gcc-arm-embedded make
```

### Windows
Install [WSL](https://learn.microsoft.com/windows/wsl/install), and run the
command below.

```bash
sudo apt update && sudo apt upgrade
```

Next, install the dependencies:

```bash
sudo apt install gcc-arm-none-eabi make
```

If you wish to use WSL instead of CoolTerm to communicate with the MD407, some
extra infrastructure is required to [attach the USB device to
WSL](https://learn.microsoft.com/windows/wsl/connect-usb) whenever you connect
the MD407 to your computer.

## Usage

Copy `Makefile` and `console` to the project root folder (where `TinyTimber.c`
is located), and follow the instructions below.

### Compiling the Project

Run `make` from the project root folder (where `TinyTimber.c` is located).

It is assumed that the Arm GNU Toolchain is on the `PATH` environment variable;
if not, edit the `TOOLCHAIN` definition in the `Makefile`, specifying the full
path including trailing slash (e.g. `$(HOME)/Downloads/arm-gnu-toolchain/bin/`).

To remove compilation artefacts, run `make clean`.

To rebuild the project from scratch, run `make new` (equivalent to `make clean`
followed by `make`).

### Communicating with the MD407

Run `make console` from the project root folder (where `TinyTimber.c` is
located), and follow the prompt.

#### Common Errors

If the `make console` connection is refused, it is likely due to file
permissions. Either run the command with superuser privileges (e.g. `sudo make
console`), or add the current user to the `dialout` group via `sudo usermod -aG
dialout $USER` (requires reboot).

If, even with superuser privileges, there is an `inappropriate ioctl for device`
error, add the current user to the `dialout` group as previously described and
reboot (after which superuser privileges are not needed).

